class Solution(object):
    def sortArrayByParity(self, A):
        """
        :type A: List[int]
        :rtype: List[int]
        """
        a = []
        b = []
        for data in A:
            if data % 2 == 0:
                a.append(data)
            else:
                b.append(data)
        return a + b
