class Solution(object):
    def diffWaysToCompute(self, input):
        """
        :type input: str
        :rtype: List[int]
        """
        if input.isdigit():
            return [eval(input)]
        else:
            ans = []
            for i in range(len(input)):
                if input[i] == '-':
                    for j in self.diffWaysToCompute(input[:i]):
                        for k in self.diffWaysToCompute(input[i+1:]):
                            ans.append(j - k)
                if input[i] == '+':
                    for j in self.diffWaysToCompute(input[:i]):
                        for k in self.diffWaysToCompute(input[i+1:]):
                            ans.append(j + k)
                if input[i] == '*':
                    for j in self.diffWaysToCompute(input[:i]):
                        for k in self.diffWaysToCompute(input[i+1:]):
                            ans.append(j * k)
            return ans