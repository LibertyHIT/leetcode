class Solution(object):
    def matrixScore(self, A):
        """
        :type A: List[List[int]]
        :rtype: int
        """
        for row_idx in range(len(A)):
            if A[row_idx][0] == 0:
                A[row_idx] = [1 - x for x in A[row_idx]]
        cnt = 0
        for col in zip(*A):
            if sum(col) < (len(col) + 1) // 2:
                for row in A:
                    row[cnt] = 1 - row[cnt]
            cnt += 1
            print(col)
        ans = 0
        for row in A:
            temp = 0
            for order, data in enumerate(row[::-1]):
                temp += data * (2 ** order)
                order += 1
            ans += temp
        return ans