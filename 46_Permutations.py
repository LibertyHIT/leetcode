class Solution(object):
    def permute(self, nums):
        """
        :type nums: List[int]
        :rtype: List[List[int]]
        """
        if len(nums) == 1:
            return [nums]
        else:
            first = nums[0]
            ans = []
            for p in self.permute(nums[1:]):
                for idx in range(len(nums)):
                    temp = p[:]
                    temp.insert(idx, first)
                    ans.append(temp)
            return ans