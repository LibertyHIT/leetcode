# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None
class Solution(object):
    def mergeTrees(self, t1, t2):
        """
        :type t1: TreeNode
        :type t2: TreeNode
        :rtype: TreeNode
        """

        def sub_merge(root1, root2):
            val = root1.val + root2.val
            node = TreeNode(val)
            if root1.left is not None and root2.left is not None:
                node.left = sub_merge(root1.left, root2.left)
            elif root2.left is None:
                node.left = root1.left
            else:
                node.left = root2.left
            if root1.right is not None and root2.right is not None:
                node.right = sub_merge(root1.right, root2.right)
            elif root2.right is None:
                node.right = root1.right
            else:
                node.right = root2.right
            return node

        ans = None
        if t1 is not None and t2 is not None:
            ans = sub_merge(t1, t2)
        elif t2 is None:
            ans = t1
        else:
            ans = t2
        return ans

