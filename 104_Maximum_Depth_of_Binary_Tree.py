# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def maxDepth(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """

        def max_dep(sub_root, level):
            if sub_root is None:
                return 0
            if sub_root.left is None and sub_root.right is None:
                return level
            elif sub_root.left is None:
                return max_dep(sub_root.right, level + 1)
            elif sub_root.right is None:
                return max_dep(sub_root.left, level + 1)
            else:
                return max(max_dep(sub_root.left, level + 1), max_dep(sub_root.right, level + 1))

        return max_dep(root, 1)
