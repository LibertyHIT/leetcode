class Solution(object):
    def addDigits(self, num):
        """
        :type num: int
        :rtype: int
        """
        if 0 <= num <= 9:
            return num
        else:
            ans = 0
            while num:
                ans += num % 10
                num //= 10
            return self.addDigits(ans)