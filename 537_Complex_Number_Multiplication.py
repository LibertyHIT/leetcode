class Solution(object):
    def complexNumberMultiply(self, a, b):
        """
        :type a: str
        :type b: str
        :rtype: str
        """

        def split_complex(comp):
            a, b = comp[:-1].split('+')
            return eval(a), eval(b)

        a_1, b_1 = split_complex(a)
        a_2, b_2 = split_complex(b)
        return str(a_1 * a_2 - b_1 * b_2) + '+' + str(a_1 * b_2 + a_2 * b_1) + 'i'
