"""
# Definition for a Node.
class Node(object):
    def __init__(self, val, children):
        self.val = val
        self.children = children
"""
class Solution(object):
    def postorder(self, root):
        """
        :type root: Node
        :rtype: List[int]
        """
        ans = []
        stack = []
        if root is None:
            return []
        else:
            stack.append(root)
            while len(stack) > 0:
                top = stack.pop(-1)
                ans.append(top.val)
                for node in top.children:
                    stack.append(node)
        return ans[::-1]
        