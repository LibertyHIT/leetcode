class Solution:
    def allPathsSourceTarget(self, graph):
        """
        :type graph: List[List[int]]
        :rtype: List[List[int]]
        """
        stack = []
        ans = []
        next_cnt = [0] * len(graph)
        stack.append(0)
        target = len(graph) - 1
        while len(stack) > 0:
            top = stack[-1]
            if next_cnt[top] > len(graph[top]) - 1:
                next_cnt[top] = 0
                stack.pop(-1)
            else:
                next_item = graph[top][next_cnt[top]]
                if next_item == target:
                    ans.append(stack + [target])
                    next_cnt[top] += 1
                else:
                    stack.append(next_item)
                    next_cnt[top] += 1
        return ans

