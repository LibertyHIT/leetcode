class Solution(object):
    def minAddToMakeValid(self, S):
        """
        :type S: str
        :rtype: int
        """
        ans = 0
        stack = []
        for c in S:
            if c == '(':
                stack.append(1)
            elif len(stack) > 0:
                stack.pop()
            else:
                ans += 1
        ans += len(stack)
        return ans