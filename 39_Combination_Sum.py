class Solution(object):
    def __init__(self):
        self.ans = []

    def combinationSum(self, candidates, target):
        """
        :type candidates: List[int]
        :type target: int
        :rtype: List[List[int]]
        """

        def sub_func(sub_list, sub_candidates, sub_target, left):
            if sub_target == 0:
                self.ans.append(sub_list)
                return
            if sub_target < sub_candidates[0]:
                return
            for i in range(left, len(sub_candidates)):
                if sub_candidates[i] > sub_target:
                    break
                tmp = sub_list + [sub_candidates[i]]
                sub_func(tmp, sub_candidates, sub_target - sub_candidates[i], i)

        candidates = sorted(candidates)
        sub_func([], candidates, target, 0)
        return self.ans


