class Solution(object):
    def numberOfLines(self, widths, S):
        """
        :type widths: List[int]
        :type S: str
        :rtype: List[int]
        """
        cur_line = 0
        cnt = 1
        for c in S:
            width = widths[ord(c) - 97]
            cur_line += width
            if cur_line > 100:
                cur_line = width
                cnt += 1
        return [cnt, cur_line]
