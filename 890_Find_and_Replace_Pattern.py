class Solution:
    def findAndReplacePattern(self, words, pattern):
        """
        :type words: List[str]
        :type pattern: str
        :rtype: List[str]
        """
        length = len(pattern)

        def check(word):
            fdic = {}
            tdic = {}
            for i in range(length):
                if pattern[i] not in fdic.keys() and word[i] not in tdic.keys():
                    fdic[pattern[i]] = word[i]
                    tdic[word[i]] = pattern[i]
                elif pattern[i] not in fdic.keys():
                    return False
                elif word[i] not in tdic.keys():
                    return False
                elif fdic[pattern[i]] != word[i] or tdic[word[i]] != pattern[i]:
                    return False
            return True

        ans = []
        for w in words:
            if check(w):
                ans.append(w)
        return ans

