# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    def __init__(self):
        self.flag = False
        self.ans = 0

    def rangeSumBST(self, root, L, R):
        """
        :type root: TreeNode
        :type L: int
        :type R: int
        :rtype: int
        """

        def search_Tree(sub_root, L, R):
            if sub_root.left is not None:
                search_Tree(sub_root.left, L, R)
            if sub_root.val == L:
                self.ans += L
                self.flag = True
            elif sub_root.val == R:
                self.ans += R
                self.flag = False
            elif self.flag is True:
                self.ans += sub_root.val
            if sub_root.right is not None:
                search_Tree(sub_root.right, L, R)

        search_Tree(root, L, R)
        return self.ans

