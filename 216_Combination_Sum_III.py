class Solution(object):
    def combinationSum3(self, k, n):
        """
        :type k: int
        :type n: int
        :rtype: List[List[int]]
        """
        ans = []
        for i in range(n + 1):
            ans.append([[]] * (k + 1))
        for i in range(1, min(n, 9) + 1):
            ans[i][1] = [[i]]
        for i in range(2, n + 1):
            for j in range(2, k + 1):
                temp = []
                for t in range(1, min(i, 9) + 1):
                    for l in range(len(ans[i - t][j - 1])):
                        if ans[i - t][j - 1][l][-1] < t:
                            temp.append(ans[i - t][j - 1][l] + [t])
                ans[i][j] = temp
        return ans[n][k]




