"""
# Definition for a Node.
class Node(object):
    def __init__(self, val, children):
        self.val = val
        self.children = children
"""


class Solution(object):
    def maxDepth(self, root):
        """
        :type root: Node
        :rtype: int
        """
        if root is None:
            return 0
        cur = [root]
        count = 0
        while len(cur) > 0:
            temp = []
            for node in cur:
                temp += node.children
            cur = temp[:]
            count += 1
        return count
                