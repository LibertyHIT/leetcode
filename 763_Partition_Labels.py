class Solution(object):
    def partitionLabels(self, S):
        """
        :type S: str
        :rtype: List[int]
        """
        dic = {}
        for i in range(len(S)):
            dic[S[i]] = i
        last_cur = 0
        cur = 0
        ans = []
        for i in range(len(S)):
            cur = max(cur, dic[S[i]])
            if i == cur:
                ans.append(cur - last_cur + 1)
                last_cur = i + 1
        return ans