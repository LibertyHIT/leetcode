class Solution:
    def constructMaximumBinaryTree(self, nums):
        """
        :type nums: List[int]
        :rtype: TreeNode
        """
        def construct_Tree(sub_nums):
            if len(sub_nums) == 0:
                return None
            elif len(sub_nums) == 1:
                return TreeNode(sub_nums[0])
            else:
                root = max(sub_nums)
                root_idx = sub_nums.index(root)
                tmp = TreeNode(root)
                tmp.left = construct_Tree(sub_nums[:root_idx])
                tmp.right = construct_Tree(sub_nums[root_idx+1:])
                return tmp
        tree = construct_Tree(nums)
        return tree