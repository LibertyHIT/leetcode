class Solution(object):
    def singleNumber(self, nums):
        """
        :type nums: List[int]
        :rtype: List[int]
        """
        xor = 0
        for num in nums:
            xor ^= num
        n = len(bin(xor)) - 3
        a, b = 0, 0
        for num in nums:
            if not num >> n & 1:
                a ^= num
            else:
                b ^= num
        return a, b
