class Solution(object):
    def pancakeSort(self, A):
        """
        :type A: List[int]
        :rtype: List[int]
        """

        def dfs(tmp_A, high=len(A)):
            if tmp_A[:high] == sorted(tmp_A[:high]):
                return []
            else:
                cur_idx = tmp_A[:high].index(high)
                tmp_A[:cur_idx + 1] = tmp_A[:cur_idx + 1][::-1]
                tmp_A[:high] = tmp_A[:high][::-1]
                return [cur_idx + 1, high] + dfs(tmp_A, high - 1)

        return dfs(A[:])

