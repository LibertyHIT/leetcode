class Solution(object):
    def kClosest(self, points, K):
        """
        :type points: List[List[int]]
        :type K: int
        :rtype: List[List[int]]
        """
        distance = []
        for point in points:
            distance.append(point[0] ** 2 + point[1] ** 2)
        ans = sorted(zip(points, distance), key=lambda x: x[1])[:K]
        return [item[0] for item in ans]