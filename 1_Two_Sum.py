class Solution(object):
    def twoSum(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: List[int]
        """
        keys=sorted(enumerate(nums), key=lambda x:x[1])
        i = 0
        j = len(nums) - 1
        while True:
            if keys[i][1] + keys[j][1] == target:
                return [keys[i][0], keys[j][0]]
            if keys[i][1] < target - keys[j][1]:
                i += 1
            elif keys[j][1] > target - keys[i][1]:
                j -= 1
            if i >= j:
                break