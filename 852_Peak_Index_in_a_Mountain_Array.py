class Solution(object):
    def peakIndexInMountainArray(self, A):
        """
        :type A: List[int]
        :rtype: int
        """
        def search(sub_indices):
            if len(sub_indices) == 1:
                return sub_indices[0]
            elif len(sub_indices) == 2:
                if sub_indices[0] > sub_indices[1]:
                    return sub_indices[0]
                else:
                    return sub_indices[1]
            else:
                length = len(sub_indices)
                mid = length // 2
                if A[sub_indices[mid - 1]] < A[sub_indices[mid]] < A[sub_indices[mid + 1]]:
                    return search(sub_indices[mid:])
                elif A[sub_indices[mid - 1]] > A[sub_indices[mid]] > A[sub_indices[mid + 1]]:
                    return search(sub_indices[:mid])
                else:
                    return sub_indices[mid]
        return search(range(len(A)))