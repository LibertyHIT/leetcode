class Solution(object):
    def sortArrayByParityII(self, A):
        """
        :type A: List[int]
        :rtype: List[int]
        """
        t1 = [i for i in A if i % 2 == 0]
        t2 = [i for i in A if i % 2 == 1]
        return [t1[i // 2] if i % 2 == 0 else t2[(i - 1) // 2] for i in range(len(A))]