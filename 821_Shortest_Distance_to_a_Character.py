class Solution(object):
    def shortestToChar(self, S, C):
        """
        :type S: str
        :type C: str
        :rtype: List[int]
        """
        length = len(S)
        ans = [20000] * length
        left = None
        for i in range(length):
            if S[i] == C:
                ans[i] = 0
                if left is None:
                    for j in range(i):
                        ans[j] = i - j
                else:
                    for j in range(left + (i - left + 1) // 2, i):
                        ans[j] = i - j
                left = i
            elif left is not None:
                if i > left:
                    ans[i] = i - left
        return ans


