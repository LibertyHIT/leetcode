class Solution(object):
    def check(self, board, i, j):
        ans = 0
        for m in range(max(0, i - 1), min(len(board), i + 2)):
            for n in range(max(0, j - 1), min(len(board[i]), j + 2)):
                if m == i and n == j:
                    continue
                try:
                    if board[m][n] & 1 == 1:
                        ans += 1
                except:
                    continue
        return ans

    def gameOfLife(self, board):
        for i in range(len(board)):
            for j in range(len(board[i])):
                record = self.check(board, i, j)
                if board[i][j] == 0 and record == 3:
                    board[i][j] = 2
                elif board[i][j] == 1 and record < 2:
                    board[i][j] = 1
                elif board[i][j] and 2 <= record <= 3:
                    board[i][j] = 3
                elif board[i][j] and record > 3:
                    board[i][j] = 1
        for i in range(len(board)):
            for j in range(len(board[i])):
                board[i][j] &= 2
                board[i][j] >>= 1

