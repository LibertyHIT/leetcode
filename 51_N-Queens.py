class Solution(object):
    def __init__(self):
        self.ans = []

    def solveNQueens(self, n):
        """
        :type n: int
        :rtype: List[List[str]]
        """
        upper_limit = (1 << n) - 1

        def recursive(row, ld, rd, board):
            if row == upper_limit:
                self.ans.append(board)
            else:
                pos = upper_limit & (~(row | ld | rd))
                while pos:
                    p = pos & (~pos + 1)
                    pos -= p
                    tmp = board[:]
                    tmp.append(p)
                    recursive(row | p, (ld | p) << 1, (rd | p) >> 1, tmp)

        recursive(0, 0, 0, [])
        real_ans = []

        def trans(line):
            cnt = 0
            while line:
                line >>= 1
                cnt += 1
            return "." * (cnt - 1) + "Q" + "." * (n - cnt)

        for k in self.ans:
            tmp = []
            for j in k:
                tmp.append(trans(j))
            real_ans.append(tmp)
        return real_ans



