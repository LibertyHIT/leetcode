class Solution(object):
    def hasAlternatingBits(self, n):
        """
        :type n: int
        :rtype: bool
        """
        return (n ^ n >> 1) == (2 ** (len(bin(n)) - 2) - 1)
