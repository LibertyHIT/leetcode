class Solution(object):
    def subsets(self, nums):
        """
        :type nums: List[int]
        :rtype: List[List[int]]
        """
        num = len(nums)
        k_list = range(1 << num)
        ans = []
        for k in k_list:
            temp = [nums[i] for i in range(num) if k & (1 << i) == (1 << i)]
            ans.append(temp)
        return ans

