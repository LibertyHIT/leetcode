# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution:
    def invertTree(self, root):
        """
        :type root: TreeNode
        :rtype: TreeNode
        """
        def invert_sub_tree(subroot):
            if subroot is None:
                return
            else:
                temp = subroot.right
                subroot.right = subroot.left
                subroot.left = temp
                invert_sub_tree(subroot.left)
                invert_sub_tree(subroot.right)
        invert_sub_tree(root)
        return root