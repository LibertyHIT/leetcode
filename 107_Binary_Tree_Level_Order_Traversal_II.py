# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def levelOrderBottom(self, root):
        """
        :type root: TreeNode
        :rtype: List[List[int]]
        """
        if root is None:
            return []
        next_nodes = []
        if root.left is not None:
            next_nodes.append(root.left)
        if root.right is not None:
            next_nodes.append(root.right)
        ans = [[root.val]]
        while len(next_nodes) > 0:
            ans.append([node.val for node in next_nodes])
            tmp = []
            for node in next_nodes:
                if node.left is not None:
                    tmp.append(node.left)
                if node.right is not None:
                    tmp.append(node.right)
            next_nodes = tmp[:]
        return ans[::-1]
