class Solution(object):
    def findWords(self, words):
        """
        :type words: List[str]
        :rtype: List[str]
        """
        row_1 = 'QWERTYUIOPqwertyuiop'
        row_2 = 'ASDFGHJKLasdfghjkl'
        row_3 = 'ZXCVBNMzxcvbnm'

        def check(word):
            c = word[0]
            if c in row_1:
                for wc in word[1:]:
                    if wc not in row_1:
                        return False
            if c in row_2:
                for wc in word[1:]:
                    if wc not in row_2:
                        return False
            if c in row_3:
                for wc in word[1:]:
                    if wc not in row_3:
                        return False
            return True

        ans = []
        for w in words:
            if check(w):
                ans.append(w)
        return ans
