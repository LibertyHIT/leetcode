class Solution(object):
    def maxIncreaseKeepingSkyline(self, grid):
        """
        :type grid: List[List[int]]
        :rtype: int
        """
        row_max = [max(i) for i in grid]
        col_max = [max(i) for i in zip(*grid)]
        ans = 0
        for i, r in enumerate(row_max):
            for j, c in enumerate(col_max):
                ans += min(r, c) - grid[i][j]
        return ans
