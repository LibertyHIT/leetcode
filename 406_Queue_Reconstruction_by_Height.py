class Solution(object):
    def reconstructQueue(self, people):
        """
        :type people: List[List[int]]
        :rtype: List[List[int]]
        """
        if len(people) <= 1:
            return people
        else:
            sorted_people = sorted(people, key=lambda x: (-x[0], x[1]))
        ans = [sorted_people[0]]
        for i in range(1, len(sorted_people)):
            lim = sorted_people[i][1]
            ans.insert(lim, sorted_people[i])
        return ans
