class Solution(object):
    def diStringMatch(self, S):
        """
        :type S: str
        :rtype: List[int]
        """
        length = len(S) + 1
        i_num = S.count('I')
        d_num = length - i_num
        i_list = list(range(i_num))
        d_list = list(range(i_num, length, 1))[::-1]
        ans = []
        i_cnt = 0
        d_cnt = 0
        for c in S:
            if c == 'I':
                ans.append(i_list[i_cnt])
                i_cnt += 1
            else:
                ans.append(d_list[d_cnt])
                d_cnt += 1
        ans.append(d_list[d_cnt])
        return ans
