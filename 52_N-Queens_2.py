class Solution(object):
    def __init__(self):
        self.ans = []

    def totalNQueens(self, n):
        """
        :type n: int
        :rtype: int
        """
        upper_limit = (1 << n) - 1

        def func(row, ld, rd, map):
            if row == upper_limit:
                self.ans.append(map)
            else:
                pos = upper_limit & (~(row | ld | rd))
                while pos:
                    p = pos & (~pos + 1)
                    pos -= p
                    func(row | p, (ld | p) << 1, (rd | p) >> 1, map)

        func(0, 0, 0)
        return self.ans
