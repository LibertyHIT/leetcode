class Solution(object):
    def titleToNumber(self, s):
        """
        :type s: str
        :rtype: int
        """
        ans = 0
        p = 1
        for c in s[::-1]:
            ans += (ord(c) - 64) * p
            p *= 26
        return ans