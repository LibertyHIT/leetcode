class Solution(object):
    def numUniqueEmails(self, emails):
        """
        :type emails: List[str]
        :rtype: int
        """
        mail_dict = {}
        for email in emails:
            user_name, host = email.split('@')
            user_name = user_name.replace('.', '').split('+')[0]
            final = user_name + '@' + host
            mail_dict[final] = 1
        return len(mail_dict.keys())
