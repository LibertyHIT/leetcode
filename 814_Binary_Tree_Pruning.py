# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def pruneTree(self, root):
        """
        :type root: TreeNode
        :rtype: TreeNode
        """

        def prune(sub_root):
            if sub_root is None:
                return True
            if sub_root.left is None and sub_root.right is None:
                return True
            elif sub_root.right is None:
                if prune(sub_root.left) and sub_root.left.val == 0:
                    sub_root.left = None
                    return True
                else:
                    return False
            elif sub_root.left is None:
                if prune(sub_root.right) and sub_root.right.val == 0:
                    sub_root.right = None
                    return True
                else:
                    return False
            else:
                print(sub_root.left.val, sub_root.right.val)
                if prune(sub_root.left) and sub_root.left.val == 0:
                    sub_root.left = None
                if prune(sub_root.right) and sub_root.right.val == 0:
                    sub_root.right = None
                if sub_root.right is None and sub_root.left is None:
                    return True
                else:
                    return False

        prune(root)
        return root