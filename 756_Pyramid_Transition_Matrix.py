from itertools import product


class Solution(object):
    def check(self, bottom, allowed):
        for tri in allowed:
            if tri[:2] == bottom:
                return True
        return False

    def find_valid(self, bottom, allowed):
        top = []
        for tri in allowed:
            if tri[:2] == bottom:
                top.append(tri[2])
        return top

    def pyramidTransition(self, bottom, allowed):
        """
        :type bottom: str
        :type allowed: List[str]
        :rtype: bool
        """
        if len(bottom) == 2:
            return self.check(bottom, allowed)
        else:
            combine = []
            for idx in range(len(bottom) - 1):
                combine.append(self.find_valid(bottom[idx:idx + 2], allowed))
            comb_length = len(combine)
            tops = list(product(*combine))
            flag = False
            for top in tops:
                next_bottom = ''.join(top)
                if self.pyramidTransition(next_bottom, allowed):
                    return True
            return False


