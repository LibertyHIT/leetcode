class RecentCounter(object):

    def __init__(self):
        self.pings = []
        self.first = 0

    def ping(self, t):
        """
        :type t: int
        :rtype: int
        """
        self.pings.append(t)
        while self.pings[self.first] < t - 3000:
            self.first += 1
        return len(self.pings) - self.first

# Your RecentCounter object will be instantiated and called as such:
# obj = RecentCounter()
# param_1 = obj.ping(t)