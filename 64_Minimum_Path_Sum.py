class Solution(object):
    def minPathSum(self, grid):
        """
        :type grid: List[List[int]]
        :rtype: int
        """
        ans = [line[:] for line in grid]
        for i in range(len(grid)):
            for j in range(len(grid[i])):
                if i > 0 and j > 0:
                    ans[i][j] = min(ans[i - 1][j], ans[i][j - 1]) + grid[i][j]
                elif i > 0:
                    ans[i][j] = ans[i - 1][j] + grid[i][j]
                elif j > 0:
                    ans[i][j] = ans[i][j - 1] + grid[i][j]
        return ans[len(grid) - 1][len(grid[len(grid) - 1]) - 1]
