# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def allPossibleFBT(self, N):
        """
        :type N: int
        :rtype: List[TreeNode]
        """
        if N == 1:
            return [TreeNode(0)]
        else:
            ans = []
            for i in range(1, N - 1, 2):
                left = self.allPossibleFBT(i)
                right = self.allPossibleFBT(N - 1 - i)
                for left_node in left:
                    for right_node in right:
                        root = TreeNode(0)
                        root.left = left_node
                        root.right = right_node
                        ans.append(root)
            return ans


