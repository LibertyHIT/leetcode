class Solution(object):
    def generate(self, numRows):
        """
        :type numRows: int
        :rtype: List[List[int]]
        """
        if numRows == 0:
            return []
        if numRows == 1:
            return [[1]]
        ans = [[1], [1, 1]]
        while len(ans) < numRows:
            lastRow = ans[-1]
            cur = [1]
            for k in range(len(lastRow) - 1):
                cur.append(lastRow[k] + lastRow[k + 1])
            cur.append(1)
            ans.append(cur)
        return ans





