"""
# Definition for a Node.
class Node(object):
    def __init__(self, val, children):
        self.val = val
        self.children = children
"""


class Solution(object):
    def levelOrder(self, root):
        """
        :type root: Node
        :rtype: List[List[int]]
        """
        if root is None:
            return []
        next_nodes = root.children
        ans = [[root.val]]
        while len(next_nodes) > 0:
            ans.append([node.val for node in next_nodes])
            tmp = []
            for node in next_nodes:
                tmp += node.children
            next_nodes = tmp[:]
        return ans

