# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.next = None

# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def sortedListToBST(self, head, tail=None):
        """
        :type head: ListNode
        :rtype: TreeNode
        """
        if head == tail:
            return None
        low = head
        high = head
        while high != tail and high.next != tail:
            low = low.next
            high = high.next.next
        root = TreeNode(low.val)
        root.left = self.sortedListToBST(head, low)
        root.right = self.sortedListToBST(low.next, tail)
        return root


