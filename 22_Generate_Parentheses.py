import itertools
class Solution(object):
    def generateParenthesis(self, n):
        """
        :type n: int
        :rtype: List[str]
        """
        ans_dict = []

        def split(k):
            if k == 2:
                return ['()']
            else:
                ans = ['(' * (k // 2) + ')' * (k // 2)]
                ans += ['(' + i + ')' for i in split(k - 2)]
                for i in range(2, k, 2):
                    for j in itertools.product(split(i), split(k-i)):
                        ans.append(j[0] + j[1])
                return list(set(ans))
        return split(n * 2)


s = Solution()
print(s.generateParenthesis(3))


