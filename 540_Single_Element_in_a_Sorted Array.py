class Solution(object):
    def singleNonDuplicate(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        acc = 0
        for num in nums:
            acc ^= num
        return acc