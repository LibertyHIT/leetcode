class Solution(object):
    def islandPerimeter(self, grid):
        """
        :type grid: List[List[int]]
        :rtype: int
        """
        ans = 0
        for i in range(len(grid)):
            for j in range(len(grid[i])):
                if j == 0 and grid[i][j] == 1:
                    ans += 1
                if j == len(grid[i]) - 1 and grid[i][j] == 1:
                    ans += 1
                try:
                    ans += grid[i][j] ^ grid[i][j + 1]
                except:
                    continue
        for i in range(len(grid)):
            for j in range(len(grid[i])):
                if i == 0 and grid[i][j] == 1:
                    ans += 1
                if i == len(grid) - 1 and grid[i][j] == 1:
                    ans += 1
                try:
                    ans += grid[i][j] ^ grid[i + 1][j]
                except:
                    continue
        return ans
