class Solution(object):
    def deckRevealedIncreasing(self, deck):
        """
        :type deck: List[int]
        :rtype: List[int]
        """
        sorted_deck = sorted(deck)
        length = len(sorted_deck)
        ans = []
        while len(sorted_deck) > 0:
            top = sorted_deck.pop(-1)
            if len(ans) > 0:
                ans.insert(0, ans.pop(-1))
            ans.insert(0, top)
        return ans