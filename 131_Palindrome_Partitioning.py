class Solution(object):
    def partition(self, s):
        """
        :type s: str
        :rtype: List[List[str]]
        """

        def dfs(res, tmp, idx):
            if idx == len(s) and len(tmp) > 0:
                res.append(tmp[:])
                return
            for i in range(idx, len(s)):
                if s[idx:i + 1] == s[idx:i + 1][::-1]:
                    tmp.append(s[idx:i + 1])
                    dfs(res, tmp, i + 1)
                    tmp.pop()

        res = []
        tmp = []
        dfs(res, tmp, 0)
        return res







