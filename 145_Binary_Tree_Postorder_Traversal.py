# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def postorderTraversal(self, root):
        """
        :type root: TreeNode
        :rtype: List[int]
        """
        if root is None:
            return []
        else:
            ans = []
            stack = [root]
            while len(stack) > 0:
                top = stack.pop()
                ans.append(top.val)
                if top.left is not None:
                    stack.append(top.left)
                if top.right is not None:
                    stack.append(top.right)
            return ans[::-1]