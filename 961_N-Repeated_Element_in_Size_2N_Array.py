class Solution(object):
    def repeatedNTimes(self, A):
        """
        :type A: List[int]
        :rtype: int
        """
        flag = [0] * 10005
        for a in A:
            if flag[a] == 0:
                flag[a] += 1
            else:
                return a