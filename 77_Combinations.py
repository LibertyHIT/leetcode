class Solution(object):
    def combine(self, n, k):
        """
        :type n: int
        :type k: int
        :rtype: List[List[int]]
        """
        ans = []
        next_cnt = [1] * (k + 1)
        stack = []
        if k == 1:
            for i in range(1, n + 1):
                ans.append([i])
        else:
            stack.append(next_cnt[len(stack)])
            next_cnt[len(stack)] += 1
            while next_cnt[0] + k - 1 <= n:
                if next_cnt[len(stack)] + k - len(stack) - 1 > n:
                    stack.pop(-1)
                    next_cnt[len(stack)] += 1
                elif len(stack) == k - 1:
                    top = stack[-1]
                    for target in range(top + 1, n + 1):
                        ans.append(stack + [target])
                    stack.pop(-1)
                    next_cnt[len(stack)] += 1
                else:
                    stack.append(next_cnt[len(stack)])
                    next_cnt[len(stack)] = stack[-1] + 1
        return ans