class Solution:
    def minDeletionSize(self, A):
        """
        :type A: List[str]
        :rtype: int
        """
        ans = 0
        for col in zip(*A):
            flag = True
            for idx in range(len(col) - 1):
                if ord(col[idx]) > ord(col[idx + 1]):
                    flag = False
                    ans += 1
                    break
        return ans
