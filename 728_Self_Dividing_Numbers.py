class Solution(object):
    def selfDividingNumbers(self, left, right):
        """
        :type left: int
        :type right: int
        :rtype: List[int]
        """

        def check(num):
            temp = num
            while temp:
                t = temp % 10
                if t == 0:
                    return False
                if num % t != 0:
                    return False
                temp = temp // 10
            return True

        ans = []
        for i in range(left, right + 1):
            if check(i):
                ans.append(i)
        return ans


