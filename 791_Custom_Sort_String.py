class Solution(object):
    def customSortString(self, S, T):
        """
        :type S: str
        :type T: str
        :rtype: str
        """
        out = ""
        ans = ""
        for c in S:
            ans += c * T.count(c)
        for c in T:
            if c not in S:
                out += c
        return ans + out

