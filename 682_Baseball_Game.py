class Solution(object):
    def calPoints(self, ops):
        """
        :type ops: List[str]
        :rtype: int
        """
        ans = 0
        stack = []
        for op in ops:
            if op == 'C':
                tmp = stack.pop()
                ans -= tmp
            elif op == 'D':
                tmp = (stack[-1]) * 2
                ans += tmp
                stack.append(tmp)
            elif op == '+':
                tmp = stack[-1] + stack[-2]
                ans += tmp
                stack.append(tmp)
            else:
                tmp = eval(op)
                stack.append(tmp)
                ans += tmp
        return ans
